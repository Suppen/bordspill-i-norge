module.exports = ({ env }) => ({
	host: env("HOST", "0.0.0.0"),
	port: env.int("PORT", 1337),
	admin: {
		auth: {
			secret: env("ADMIN_JWT_SECRET", "b011e47d0bf4b33ed8a6ef1eabbf87dd")
		}
	}
});
