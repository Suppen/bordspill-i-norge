export interface TabletopType {
	id: number;
	name_nb: string;
	name_en: string;
}

export interface Region {
	id: number;
	name_nb: string;
	name_en: string;
}

export interface Location {
	id: number;
	name: string;
	address?: string;
	latitude: number;
	longitude: number;
	associations?: Association[];
}

export interface Association {
	id: number;
	name: string;
	description: string;
	url: string;
	region: Region;
	locations?: Location[];
	tabletop_types?: TabletopType[];
}

/** Gets the location objects of an association */
export const getLocations = ({ locations }: Association): Location[] => locations ?? [];

/** Gets the tabletop types of an association */
export const getTabletopTypes = ({ tabletop_types }: Association): TabletopType[] => tabletop_types ?? [];
