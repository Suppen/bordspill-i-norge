import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { icon } from "leaflet";
import "./AssociationsMap.scss";
import { Association, getLocations } from "./Association";

export interface AssociationsMapProps {
	associations: Association[];
}

const markerIcon = icon({
	iconUrl: "/img/markers/marker-icon.png",
	iconRetinaUrl: "/img/markers/marker-icon-2x.png",
	shadowUrl: "/img/markers/marker-shadow.png",
	iconAnchor: [12.5, 41]
});

const AssociationsMap = ({ associations }: AssociationsMapProps): JSX.Element => {
	return (
		<MapContainer center={[65.257, 16.392]} zoom={4}>
			<TileLayer
				attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
				url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
			/>
			{associations.map(a =>
				getLocations(a).map(l => (
					<Marker key={a.id} icon={markerIcon} position={[l.latitude, l.longitude]}>
						<Popup>
							<p>{a.name}</p>
							<p>
								<a href={a.url} target="_blank" rel="noreferrer noopener">
									{a.url}
								</a>
							</p>
						</Popup>
					</Marker>
				))
			)}
		</MapContainer>
	);
};

export default AssociationsMap;
