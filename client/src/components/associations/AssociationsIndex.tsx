import React from "react";
import * as R from "ramda";
import env from "../../env";
import AssociationsMap from "./AssociationsMap";
import useSWR from "swr";
import ReactMarkdown from "react-markdown";
import { Association, getLocations, getTabletopTypes } from "./Association";

const AssociationsIndex = (): JSX.Element | null => {
	const { data: associations } = useSWR<Association[], unknown>(`${env.REACT_APP_API_BASE_URL}/associations`);

	if (associations === undefined) {
		return null;
	}

	const associationsByRegion = R.groupBy(a => a.region.name_nb, associations);

	return (
		<div className="row">
			<div className="col-12">
				<h1>Foreninger</h1>
			</div>
			<div className="col-12">
				<AssociationsMap associations={associations} />
			</div>
			<div className="col-12">
				<ul className="list-unstyled">
					{Object.keys(associationsByRegion).map(region => (
						<li key={region}>
							<h2 className="text-capitalize">{region}</h2>
							<ul>
								{associationsByRegion[region].map(a => (
									<li key={a.id}>
										<a href={a.url} target="_blank" rel="noreferrer">
											<h3 className="pt-0">{a.name}</h3>
										</a>
										<ReactMarkdown>{a.description}</ReactMarkdown>
										{getLocations(a).length > 0 ? (
											<p>
												Holder til på:
												<ul>
													{getLocations(a).map(l => (
														<li key={l.id}>
															<a
																href={`https://www.google.com/maps/search/?api=1&query=${l.latitude},${l.longitude}`}
																target="_blank"
																rel="noreferrer"
															>
																{l.name}
															</a>
														</li>
													))}
												</ul>
											</p>
										) : null}
										{getTabletopTypes(a).length > 0 ? (
											<p>
												Driver med:
												<ul>
													{getTabletopTypes(a).map(t => (
														<li key={t.id} className="text-capitalize">
															{t.name_nb}
														</li>
													))}
												</ul>
											</p>
										) : null}
									</li>
								))}
							</ul>
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export default AssociationsIndex;
