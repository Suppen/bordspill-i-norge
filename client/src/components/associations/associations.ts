type Region = "vest" | "øst" | "sør" | "midt" | "nord";
type TabletopType = "boardgames" | "rpgs" | "miniatures";

export interface Association {
	id: string;
	name: string;
	description: string;
	url: string;
	locations: {
		name: string;
		latitude: number;
		longitude: number;
	}[];
	region: Region;
	types: TabletopType[];
}

const associations = [
	{
		id: "1",
		name: "Bergen Brettspillklubb",
		description:
			"Bergen Brettspillklubb holder til på Nordnes i Bergen. Vi samles hver onsdag kl. 17:30 - 23:00 og spiller alt mulig rart av brettspill.",
		url: "https://bergenbrettspill.no",
		locations: [
			{
				name: "Nordnes Bydelshus",
				latitude: 60.3946802,
				longitude: 5.3157323
			}
		],
		region: "vest",
		types: ["boardgames"]
	},
	{
		id: "2",
		name: "LURDO!",
		description:
			"LURDO! er gruppa for alle spillinteresserte LURinger, uansett spillinteresse. Vi samles minst en gang i måneden og spiller diverse kort-, tv- og brettspill. LURDO! er en lavterskels arena, der man møtes på tvers av kull og andre hobbyer, og spiller spill sammen.",
		url: "https://spanskroret.no/lurdo/",
		locations: [
			{
				name: "NTNU",
				latitude: 63.419149,
				longitude: 10.402075
			}
		],
		region: "midt",
		types: ["boardgames", "rpgs"]
	},
	{
		id: "3",
		name: "Mosjøen Spillforening",
		description:
			"Mosjøen Spillforening er en interesseorganisasjon for spillinteresserte i Mosjøen og omegn.",
		url: "https://www.facebook.com/Mosjoenspillforening",
		locations: [
			{
				name: "Olderskog Skole",
				latitude: 65.830052364251,
				longitude: 13.22499604958
			}
		],
		region: "midt",
		types: ["rpgs", "miniatures", "boardgames"]
	},
	{
		id: "4",
		name: "Spillekveld på 37 :)",
		description:
			"Spillekveld på 37 :) er en lokal spillklubb i horten som består av medlemmer i mange aldersgrupper. Vi spiller brettspill, rollespill og magic the gathering en gang i uken på Kulturhus 37 i horten fra 17-21! aldersgrense 13-uendelig (ned til 10 i følge av voksen, som da må være med å spille sammen med barnet). om du er nysgjerrig, rutinert, eller veteran spiller rolle (😂), bare kom å lek med oss da vel!!",
		url: "https://www.facebook.com/groups/175323049161024/",
		locations: [
			{
				name: "Kulturhuset 37",
				latitude: 59.4145853,
				longitude: 10.48125
			}
		],
		region: "øst",
		types: ["boardgames", "rpgs"]
	}
] as Association[];

export default associations;
