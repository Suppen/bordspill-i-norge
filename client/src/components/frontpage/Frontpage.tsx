import React from "react";

const Frontpage = (): JSX.Element => (
	<div className="row">
		<div className="col-12">
			<h1>Bordspillforbundet</h1>
			<p>
				<a href="https://n4f.no" target="_blank" rel="noopener noreferrer">
					Hyperion
				</a>{" "}
				bytter organisasjonsmodell, og ønsker seg en særforbundsmodell. Særforbundene vil erstatte dagens
				regionslag. Dette for at foreninger skal kunne forholde seg til noen med mer spisset kompetanse mot det
				akkurat de driver med. En paintballforening har nemlig vidt forskjellige behov fra det en
				brettspillforening har.
			</p>
			<p>Vi ønsker å være særforbundet til alle som driver med bordspill.</p>
			<p>
				Med bordspill menes spill som spilles oppå vanlige bord, ikke spill som krever spesielle bord. Det
				inkluderer bl.a.
			</p>
			<ul>
				<li>Brettspill</li>
				<li>Rollespill</li>
				<li>Kortspill</li>
				<li>Miniatyrspill</li>
			</ul>
			<p>
				Vi ønsker å knytte sammen bordspillinteresserte fra hele landet. Vi skal hjelpe foreninger å få
				pengestøtte, finne lokaler, og å rekruttere medlemmer. Det ultimate målet er at det skal bli like enkelt
				å finne en forening som driver med bordspill som det er å finne en som driver med idrett.
			</p>
			<p>Bordspill er så mye mer enn Ludo og Monopol. Det ønsker vi å vise Norge!</p>
			<p>Våre mål er:</p>
			<ul>
				<li>Knytte sammen bordspillinteresserte over hele landet</li>
				<li>Tilrettelegge community for utvikling og ressurser av bordspill i Norge</li>
				<li>
					Tilrettelegge for forening-til-forening-kommunikasjon slik at vi enklere får knyttet effektive
					nettverk
				</li>
				<li>Arrangere eller tilrettelegge for turneringer. Gi støtte til slikt</li>
				<li>Bordspillmarket. Kjøp, salg og bytte</li>
				<li>Jobbe for å få flere bordspill oversatt til norsk</li>
				<li>Bidra til rekruttering, og tilgjengeliggjøre informasjon om foreningene på nettet</li>
				<li>Vise Norge at bordspill er langt mer enn Ludo og Monopol</li>
			</ul>
		</div>
	</div>
);

export default Frontpage;
