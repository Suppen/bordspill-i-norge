import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from "./Navbar";
import Frontpage from "../frontpage/Frontpage";
import AssociationsIndex from "../associations/AssociationsIndex";
import "./App.scss";

const App = (): JSX.Element => (
	<div className="App">
		<div className="container">
			<BrowserRouter>
				<Navbar />
				<Switch>
					<Route exact path="/">
						<Frontpage />
					</Route>
					<Route exact path="/foreninger">
						<AssociationsIndex />
					</Route>
				</Switch>
			</BrowserRouter>
		</div>
	</div>
);

export default App;
