import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faDiscord, faFacebook } from "@fortawesome/free-brands-svg-icons";
import "./Navbar.scss";

const Navbar = (): JSX.Element => {
	const location = useLocation();

	const navItems = [
		{
			label: "Foreninger",
			path: "/foreninger"
		}
	] as { label: string; path: string }[];

	return (
		<div className="navbar navbar-expand-lg navbar-light bg-light">
			{/* Brand */}
			<Link className="navbar-brand" to="/">
				<img src="/img/Logo.png" alt="logo" />
				Bordspillforbundet
			</Link>
			{/* Toggler */}
			<button
				className="navbar-toggler"
				type="button"
				data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent"
				aria-expanded="false"
				aria-label="Toggle navigation"
			>
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="navbarSupportedContent">
				<ul className="navbar-nav mr-auto">
					{navItems.map(({ label, path }, i) => (
						<li key={i} className={`nav-item ${location.pathname === path ? "active" : ""}`}>
							<Link to={path} className="nav-link">
								{label}
							</Link>
						</li>
					))}
					<li className="nav-item dropdown">
						<a
							className="nav-link dropdown-toggle"
							data-toggle="dropdown"
							href="#"
							role="button"
							aria-haspopup="true"
							aria-expanded="false"
						>
							Finn oss på...
						</a>
						<div className="dropdown-menu">
							<a
								className="dropdown-item"
								href="https://discord.gg/UAHtEtjfyd"
								target="_blank"
								rel="noopener noreferrer"
							>
								<FontAwesomeIcon icon={faDiscord} style={{ color: "#7289da" }} /> <span>Discord</span>
							</a>
							<a
								className="dropdown-item"
								href="https://www.facebook.com/Bordspillforbundet/"
								target="_blank"
								rel="noopener noreferrer"
							>
								<FontAwesomeIcon icon={faFacebook} style={{ color: "#3b5998" }} /> <span>Facebook</span>
							</a>
							<a className="dropdown-item" href="mailto:bordspillforbundet@gmail.com">
								<FontAwesomeIcon icon={faEnvelope} /> <span>E-post</span>
							</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	);
};

export default Navbar;
