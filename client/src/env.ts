import * as yup from "yup";

const envSchema = yup
	.object({
		REACT_APP_API_BASE_URL: yup.string().url().required()
	})
	.required();

const env = envSchema.validateSync(process.env);

export default env;
