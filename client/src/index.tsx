import React from "react";
import ReactDOM from "react-dom";
import App from "./components/application/App";
import "bootstrap";
import "./index.scss";

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById("root")
);
